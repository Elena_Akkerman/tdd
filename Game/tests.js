describe('Game', function(){
	describe('#rand_numb',function(){
		it('value should not be bigger than 10',function(){
			var game = new Game(), result = [];
			for(var i=0; i<100;i++){
				result[i] = game.rand_numb();
			} 
			result.every(function(item){return(result<=10 && result>=1)}).should.be.true;
		});		
	});
	describe('#make_guess',function(){
		it('if guess is right',function(){
			var game = new Game(), result;
			result = game.make_guess(game.guess_number);
			result.should.be.true; 
		});
		it('if guess is wrong',function(){
			var game = new Game(), result;
			result = game.make_guess(game.guess_number);
			result.should.be.false; 
		});
		it('if guessed number is within 1..10',function(){
			var game = new Game(), result = [];
			for(var i=0; i<100;i++){
				result[i] = game.make_guess(game.guess_number);
			} 
			result.every(function(item){return(result<=10 && result>=1)}).should.be.true; 
		});
	});
	describe('#hint',function(){
		it('displays hint',function(){
			var game = new Game(), result;
			result = game.hint();
			result.should.be.ok;
		});
	});
	describe('#end_of_game', function(){
		it('the player wins', function(){
			var game = new Game(), result;
			result = function(){
				return(game.make_guess(game.guess_number)==rand_numb);
			}
			result.should.be.true;
		});
		it('three attempts were made', function(){
			var game = new Game(), result;
			game.make_guess(game.right_number-1);
			game.make_guess(game.right_number-1);
			game.make_guess(game.right_number-1);
			result = game.end_of_game();
			result.should.be.true;
		});
	});
});



